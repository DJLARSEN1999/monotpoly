import React, { useState } from 'react';
import Project from './Project';

const ProjectList: React.FC = () => {
  const [projects, setProjects] = useState<Array<ProjectData>>([
    {
      title: "Leap Frog",
      message: "This is a project I created for TechWise.  It is a rendition of the classic Frogger game. I added the logs to this project and the functionality of the frog riding the logs. I also managed the GitHub merge conflicts for this project.",
      repositoryUrl: 'https://github.com/JLopes20/Techwise_Leap_frog',
      websiteUrl: undefined
    },
    {
      title: "Pantry Cookbook",
      message: "This is a project I created for TechWise. It is a pantry recipe app designed to help users search and save recipes using ingredients they already have on hand and to make healthy foods more affordable. I created the Home Page and the Extra Helpings page for this project as well as connecting to the API, creating and connecting to the database, and deployment to Azure as well as managing the repository.",
      repositoryUrl: 'https://github.com/JenniferLarsen/PantryCookbook',
      websiteUrl: 'https://impastapantrycookbooks.azurewebsites.net/'
    }
  ]);

  const addProject = () => {
    // Example of adding a new project dynamically
    const newProject: ProjectData = {
      title: "New Project",
      message: "This is a new project added dynamically.",
      repositoryUrl: 'https://github.com/',
      websiteUrl: undefined
    };
    setProjects([...projects, newProject]);
  };

  return (
    <>
      <button onClick={addProject}>Add Project</button>
      {projects.map((project, index) => (
        <Project
          key={index}
          title={project.title}
          message={project.message}
          repositoryUrl={project.repositoryUrl}
          websiteUrl={project.websiteUrl}
        />
      ))}
    </>
  );
};

export default ProjectList;

interface ProjectData {
  title: string;
  message?: string;
  repositoryUrl: string;
  websiteUrl?: string;
}