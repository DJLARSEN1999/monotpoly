// Project.tsx
import React from 'react';

type ProjectProps = {
  title: string;
  message?: string;
  repositoryUrl: string;
  websiteUrl?: string; 
};

const Project: React.FC<ProjectProps> = ({ title, message, repositoryUrl, websiteUrl }) => {
  return (
    <>
      <h1>{title}</h1>
      <p>{message}</p>
      <div>
        <a href={repositoryUrl} target="_blank" rel="noopener noreferrer">Repository</a>
        {websiteUrl && (
          <>
            {' | '}
            <a href={websiteUrl} target="_blank" rel="noopener noreferrer">Website</a>
          </>
        )}
      </div>
    </>
  );
};

export default Project;
