import React from 'react';

const MainTitle: React.FC = () => {
  return (
    <header>
      <h1>Welcome to the Project Portfolio for Jennifer Larsen</h1>
    </header>
  );
};

export default MainTitle;