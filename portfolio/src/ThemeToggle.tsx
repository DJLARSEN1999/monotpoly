import React from 'react';

type ThemeToggleProps = {
  toggleTheme: () => void;
};

const ThemeToggle: React.FC<ThemeToggleProps> = ({ toggleTheme }) => (
  <div className="ThemeToggle">
    <button onClick={toggleTheme}>Toggle Theme</button>
  </div>
);


export default ThemeToggle;