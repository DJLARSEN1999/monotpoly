import React, { useState, useEffect } from 'react';
import MainTitle from './MainTitle';
import ProjectList from './ProjectList';
import ThemeToggle from './ThemeToggle';
import './App.css';

const App: React.FC = () => {
  const [theme, setTheme] = useState('light');

  useEffect(() => {
    document.body.classList.remove('light', 'dark');
    document.body.classList.add(theme);
  }, [theme]);

  const toggleTheme = () => {
    setTheme((prevTheme) => (prevTheme === 'light' ? 'dark' : 'light'));
  };

  return (
    <div className="App">
      <MainTitle />
      <ThemeToggle toggleTheme={toggleTheme} />
      <main>
        <ProjectList />
      </main>
    </div>
  );
};

export default App;