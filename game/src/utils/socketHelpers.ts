import { Socket } from "socket.io";

export const emitError = (socket: Socket, message: string) => socket.emit(message);