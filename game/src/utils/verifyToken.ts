import axios from 'axios';
import { response } from 'express';
// Change back to 'http://127.0.0.1:6001' if localhost doesn't work
//const AUTH_SERVER_URL = 'http://127.0.0.1:6001';

const AUTH_SERVER_URL = 'http://127.0.0.1:6001';

export interface AuthResponse {
    username: string;
    isValid: boolean;
}

const verifyToken = async (token: string): Promise<AuthResponse> => {
    try {
        const response = await axios.get(`${AUTH_SERVER_URL}/verify`, {
            headers: {
                // Send the token as a Bearer token
                Authorization: `Bearer ${token}`,
            },
        }
        );
        return response.data as AuthResponse;
    } catch (e) {
        console.error('Error verifying token', e);
        return { username: '', isValid: false };
    }
};
export default verifyToken;