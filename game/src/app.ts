import express from "express";
import http, { createServer } from "http";
import { Server as ioServer } from "socket.io";
import cors from "cors"; 
import mongoose from "mongoose";
import authMiddleware from "./middleware/authMiddleware";
import {initSocketHandlers } from "./socketHandlers";
import { join } from "path";

const PORT = 6002;
const ioOptions = {
    cors: {
        origin: "*",
    },
};
 
const app = express();
app.use(cors());
const server = http.createServer(app);
const io = new ioServer(server, ioOptions);

app.use(express.static("public"));

io.use(authMiddleware);
initSocketHandlers(io);

app.get("/", (req, res) => {
    res.sendFile(join(__dirname, '../public/test.html'));
});

async function main() {
    await mongoose.connect("mongodb://127.0.0.1:27017/MonotpolyGames");
    server.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
}

main();