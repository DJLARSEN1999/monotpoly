import { Socket } from 'socket.io';
import { emitError } from './utils/socketHelpers';

interface Player {
    socketId: string;
    username: string;
    roomId: string;
}
const rooms: { [roomId: string]: Player[] } = {};

const maxPlayers = 4;

const generateRoomId = (): string => Math.random().toString(36).substring(7);
const isRoomEmpty = (roomId: string) => rooms[roomId].length === 0;
const isRoomFull = (roomId: string) => rooms[roomId].length >= maxPlayers;

export const doesRoomExist = (roomId: string) => rooms.hasOwnProperty(roomId);

export const createRoom = async (socket: Socket): Promise<void> => {
    const roomId = generateRoomId();

    rooms[roomId] = [];

    socket.emit('room_created', roomId);

    await joinRoom(socket, roomId);
    console.log('rooms', rooms);
};

export const joinRoom = async (socket: Socket, roomId: string): Promise<void> => {
    if (!doesRoomExist(roomId)) {
        emitError(socket, 'Room does not exist');
        return;
    }
    if (isRoomFull(roomId)) {
        emitError(socket, 'Room is full.');
        return;
    }
    const player: Player = {
        socketId: socket.id,
        username: socket.data.username,
        roomId,
    };
    rooms[roomId].push(player);
    socket.join(roomId);
    socket.emit('room_joined', roomId);
    socket.to(roomId).emit('player_joined', player.username);
    socket.data.roomId = roomId;
}

export const leaveRoom = (socket: Socket): void => { 
    const { roomId } = socket.data;
    if (roomId && doesRoomExist(roomId)) {
        rooms[roomId] = rooms[roomId].filter(player => player.socketId != socket.id);
        if (isRoomEmpty(roomId)) {
            delete rooms[roomId];
        } else {
            socket.to(roomId).emit('player_left', socket.id);
        }
        socket.leave(roomId);
        socket.data.roomId = '';
        }
    }