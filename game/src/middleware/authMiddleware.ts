import { Socket } from 'socket.io';
import verifyToken from '../utils/verifyToken';
import { ExtendedError } from 'socket.io/dist/namespace';

const authMiddleware = async (socket: Socket, next: (err?: ExtendedError) => void) => {
    try {
        const token = socket.handshake.auth.token as string;
        const response = await verifyToken(token);
        if (response.isValid) {
            console.log('User is authenticated', response.username);
            socket.data.username = response.username;
            next();
        } else {
            console.log('Invalid token');
            next(new Error('Invalid token'));
        }
    } catch (e) {
    next(new Error('Internal server error'));
    }
};
export default authMiddleware;