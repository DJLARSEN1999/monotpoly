import  { Server, Socket } from 'socket.io';
import { createRoom, joinRoom, leaveRoom, doesRoomExist } from './roomManager';
import { createBrotliCompress } from 'zlib';
import { emitError } from './utils/socketHelpers';
import { isObjectIdOrHexString } from 'mongoose';

export const initSocketHandlers = (io: Server) => {
    io.on('connection', (socket: Socket) => {
        console.log('User connected', socket.data.username);
        socket.data.roomId = '';
        
        socket.on('create_room', () => createRoom(socket));
        socket.on('join_room', (roomId) => joinRoom(socket, roomId));
        socket.on('leave_room', () => leaveRoom(socket));

        socket.on('chat_message', (message: string) => handleChatMessage(io, socket, message));

        socket.on('disconnect', () => {
            leaveRoom(socket);
            console.log('User disconnected', socket.id);
        });
    });
}

const handleChatMessage = async (io: Server, socket: Socket, message: string) => {
    const { roomId } = socket.data;
    if (!doesRoomExist(roomId)) {
        emitError(socket, 'room not found');
        return;
    }
    io.to(roomId).emit('chat_message', { username: socket.data.username, message });
}
