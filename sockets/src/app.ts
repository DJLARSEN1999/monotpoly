import express from 'express';
import { createServer } from 'http';
import { Server as ServerIO, Socket } from 'socket.io';
import { join } from 'path';

const app = express();
const server = createServer(app);
const io = new ServerIO(server);

app.get('/', (req, res) => {
    res.sendFile(join(__dirname, '../public/index.html'));
});

io.on('connection', (socket: Socket) => {
    console.log('User connected');
    socket.join('room1');
    socket.data.room = 'room1';
    socket.on('disconnect', () => {
        console.log('User disconnected');
    });
    socket.on('chat message', (msg: string) => {
        io.to(socket.data.room).emit('chat message', msg);
        console.log('message: ' + msg);
    });
});

function main() {
    server.listen(3000, () => {
        console.log('Server running on port 3000');
    });
}
main()
