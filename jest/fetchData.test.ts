import { describe, expect, test } from '@jest/globals';
import fetchData from './fetchData';

test('the data is peanut butter', async () => {
    await expect(fetchData()).resolves.toBe('peanut butter');
  }, 10000); // Increase timeout to 10 seconds
  
  test('the fetch fails with an error', async () => {
    await expect(fetchData()).rejects.toThrow('error');
  });
  
  test('the data is peanut butter', done => {
    function callback(error, data) {
      if (error) {
        done(error); // Pass error to done() directly
      } else {
        try {
          expect(data).toBe('peanut butter');
          done();
        } catch (error) {
          done(error);
        }
      }
    }
  
    fetchData(callback);
  });
  
  test('the data is peanut butter', () => {
    return expect(fetchData()).resolves.toBe('peanut butter');
  });
  
  test('the fetch fails with an error', () => {
    return expect(fetchData()).rejects.toThrow('error');
  });