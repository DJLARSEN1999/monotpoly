/* eslint-disable no-undef */
describe("authenticating to the app", () => {
    it("should be able to register a user", () => {
        cy.visit("/");
        cy.contains("I need to login");
        cy.get("input[name=username]").type("cypress");
        cy.get("input[name=password]").type("c");
        cy.get("input[name=confirmPassword]").type("c");
        cy.contains("Register").click();
    });
    it.only("should be able to log a user in", () => {
        cy.visit("/");
        cy.contains("I need to login").click();
        cy.get("input[name=username]").type("cypress");
        cy.get("input[name=password]").type("c");
        cy.contains("log in").click();
        cy.contains("Welcome to Monotpoly");
    });
    it.only("should use the custom login command to drive me to work", () => {
        cy.login("cypress", "c");
        cy.pause();
        // now we develop our app, perhaps even using test driven development
    });
});