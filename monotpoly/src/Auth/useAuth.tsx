import { createContext, useState, useContext } from "react";

interface User {
    name: string;
    authToken?: string;
}
interface AuthContext {
    user: User | null;
    setUser: (user: User | null) => void;
}
//initialize auth context
const AuthContext = createContext<AuthContext>({
    user: null,
    setUser: () => {},
});

const useProvideAuth = () => {
    const [user, setUser] = useState<User | null>(null);
    return { user, setUser };
};
// provider component wraps App allowing it and any children to useAuth
export function ProvideAuth({ children }: { children: JSX.Element }) {
    const auth = useProvideAuth();
    return <AuthContext.Provider value={auth}> {children}</AuthContext.Provider>;
}
// allows components downstream of provider to use (consume) auth context
export const useAuth = () => {
    return useContext(AuthContext);
};