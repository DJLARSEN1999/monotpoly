import { useReducer } from "react";
import { gameboard } from "../board";
import { buyProperty, endTurn, step } from "./monotpolyReducers";

import {
  Player,
  SpaceData,
  PropertySpaceData,
  StatefulSpace,
  StatefulPropertySpace,
  GameState,
} from "../common/types";

const player: Player = {
  name: "Jenny",
  token: "♟︎",
  color: "purple",
  money: 400,
  currentPosition: 0,
  ownedProperties: [],
};
const player2: Player = {
  ...player,
  name: "Doug",
  color: "orange",
  ownedProperties: [],
};

function makeStateful(space: SpaceData | PropertySpaceData) {
  const statefulSpace: StatefulSpace | StatefulPropertySpace = {
    ...space,
    players: [],
  };
  return statefulSpace;
}
const initialState: GameState = {
  board: gameboard.map(makeStateful),
  players: [player, player2],
  turn: {
    currentPlayerHasMoved: false,
    currentPlayerIndex: 0,
    offerToBuy: false,
  },
};
initialState.board[0].players = [player, player2];

interface MonotpolyAction {
  type: string;
}

function monotpolyReducer(
  state: GameState,
  action: MonotpolyAction
): GameState {
  switch (action.type) {
    case "step": {
      const newState = step(state);
      return { ...newState };
    }
    case "endTurn": {
      return { ...endTurn(state) };
    }
    case "buyProperty": {
      return { ...buyProperty(state) };
    }
  }
  return { ...state };
}

export default function useMonotpoly() {
  const [state, dispatch] = useReducer(monotpolyReducer, initialState);
  const actions = {
    step: () => dispatch({ type: "step" }),
    endTurn: () => dispatch({ type: "endTurn" }),
    buyProperty: () => dispatch({ type: "buyProperty" }),
  };
  return { state, actions };
}
