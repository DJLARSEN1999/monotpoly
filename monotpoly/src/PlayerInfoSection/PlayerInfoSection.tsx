import { useAuth } from "../Auth/useAuth";
import { Player, TurnState } from "../common/types";
import classes from "./PlayerInfoSection.module.css";

interface PlayerInfoSectionProps {
  players: Player[];
  turn: TurnState;
}

export default function PlayerInfoSection({
  players,
}: PlayerInfoSectionProps) {
  return (
    <div>
      this is the player info section
      {players.map((player) => (
        <PlayerInfoCard {...player} key={player.name} /> 
      ))}
    </div>
  );
}

function PlayerInfoCard({ name, money, ownedProperties }: Player) {
  const { user } = useAuth();
  const itsMe = user?.name == name;
  return (
      <div className={classes.card}>
      <h2 className={itsMe ? classes.me : classes.name}>{name}</h2>
      <p>${money}</p>
      <ul>
        {ownedProperties.map((op, index) => ( 
          <li key={index} className={classes.bought} style={{ backgroundColor: op.region }}>
            {op.name}
          </li>
        ))}
      </ul>
    </div>
  );
}