import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

const LOCAL_AUTH = "http://localhost:6001"

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: "./src/tests/setup.js",
  },
  server: {
    port: 6006,
    proxy: {
      "/register": LOCAL_AUTH,
      "/login": LOCAL_AUTH
    },
  },
});
