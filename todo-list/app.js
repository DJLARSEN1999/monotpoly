const express = require('express');
const app = express();
const port = 3000;

let todoData = [
  { id: 1, title: 'Buy milk', completed: false },
  { id: 2, title: 'Feed cat', completed: true },
  { id: 3, title: 'Do homework', completed: false }
];

app.get('/todos', (req, res) => {
  res.json(todoData);
});

app.post('/add-todo', (req, res) => {
  const newTodo = {
    id: todoData.length + 1,
    title: req.query.title,
    completed: false
  };
  todoData.push(newTodo);
  res.json(newTodo);
});

app.delete('/delete-todo', (req, res) => {
  const id = req.query.id;
  todoData = todoData.filter(todo => todo.id !== Number(id));
  res.json({ id });
});

app.put('/update-todo', (req, res) => {
  const id = req.query.id;
  const todo = todoData.find(todo => todo.id === Number(id));
  todo.completed = !todo.completed;
  res.json(todo);
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
