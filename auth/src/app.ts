import express, { Application, Request, Response } from "express";
import mongoose from "mongoose";
import User from "./models/User";
import jwt from "jsonwebtoken";
import { expressjwt, Request as JWT_SECRET } from "express-jwt";
import cors from "cors";
const app: Application = express();
const PORT = 6001;
const JWT_SECRET = "planetKrypton";

// const express = require('express');
const { createServer } = require('node:http');
const { join } = require('node:path');

// const app = express();
const server = createServer(app);

app.get('/', (req, res) => {
  res.sendFile(join(__dirname, 'index.html'));
});

server.listen(3000, () => {
  console.log('server running at http://localhost:3000');
});

app.use(cors());
app.use(express.urlencoded({ extended: false }), express.json());
app.get("/", (req, res) => {
  res.send("hello world with typescript except the build breaking!");
});

app.post("/register", async (req: Request, res: Response) => {
  try {
      const { username, password } = req.body;
      const user = await new User({ username, password });
      const after = await user.save();
      res.json({ userCreated: after.username });
  } catch (e) {
      res.sendStatus(500);
  }
});

app.post("/login", async (req, res) => {
  const { username, password } = req.body;
  const user = await User.findOne({ username });
  if (!user) return(res.sendStatus(404));
  const handler = (isValidPassword: boolean) => {
    if (isValidPassword) {
      res.json({
        jwt: jwt.sign({ user: user!.username }, JWT_SECRET, {
          expiresIn: "6h",
        }),
      });
    } else {
      res.sendStatus(401);
    }
  };
  user!.validatePassword(password, handler);
});

app.get(
  "/verify",
  expressjwt({ secret: JWT_SECRET, algorithms: ["HS256"] }),
  (req: JWT_SECRET, res) => {
    console.log(req.auth);
    // if (req.auth?.user) res.sendStatus(200);
    if (req.auth?.user) res.json({ username: req.auth?.user, isValid: true });
    else res.sendStatus(401);
  }
);

async function main() {
  await mongoose.connect("mongodb://127.0.0.1:27017/course3auth");
  app.listen(PORT, () => console.log(`Auth server is listening on ${PORT}`));
}

main();

