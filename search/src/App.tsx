import React, { useState } from 'react';
import './App.css';

interface TitleProps {
  name: string;
}

interface SearchResult {
  title: string;
  url: string;
  description: string;
}

// Reusable component for the title
const Title: React.FC<TitleProps> = ({ name }) => {
  return <div className="roboto-bold">{name}</div>;
};

// Reusable component for the search box
const SearchBox: React.FC<{ onSearch: (term: string) => void }> = ({ onSearch }) => {
  const [searchTerm, setSearchTerm] = useState("react");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onSearch(searchTerm);
  };

  return (
    <form className="searchBox" onSubmit={handleSubmit}>
      <i className="material-icons">search</i>
      <input
        type="text"
        placeholder="Search Google"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
    </form>
  );
};

// Reusable component for the search buttons
function SearchButtons() {
  return (
    <div>
      <button className="searchButton">Google Search</button>
      <button className="luckyButton">I'm feeling lucky</button>
    </div>
  );
}

// Reusable component for a single search result item
function SearchResultItem({ title, url, description }) {
  return (
    <div className="searchResultItem">
      <a href={url} className="searchResultTitle" target="_blank" rel="noopener noreferrer">{title}</a>
      {description && <p className="searchResultDescription">{description}</p>}
    </div>
  );
}

// Reusable component for the search results list
function SearchResultsList({ results }: { results: SearchResult[] }) {
  return (
    <div className="searchResults">
      {results.map((result, index) => (
        <SearchResultItem
          key={index}
          title={result.title}
          url={result.url}
          description={result.description}
        />
      ))}
    </div>
  );
}

// People Also Ask section component
const AlsoAskSection: React.FC<{ results: SearchResult[] }> = ({ results }) => {
  const [isExpanded, setIsExpanded] = useState(false);

  return (
    <div className="alsoAskSection">
      <h2 onClick={() => setIsExpanded(!isExpanded)}>
        People Also Ask
        <span className={`arrow ${isExpanded ? 'expanded' : 'collapsed'}`}></span>
      </h2>
      {isExpanded && (
        <div className="alsoAskList">
          {results.map((result, index) => (
            <SearchResultItem
              key={index}
              title={result.title}
              url={result.url}
              description={result.description}
            />
          ))}
        </div>
      )}
    </div>
  );
};


function App() {
  const [searchTerm, setSearchTerm] = useState("react");
  const [searchResults, setSearchResults] = useState([] as SearchResult[]);

  const handleSearch = (term: string) => {
    // Update search term
    setSearchTerm(term);

    // Set search results based on the selected option
    if (term === "react") {
    setSearchResults([
    {
      title: "React",
      url: "https://react.dev",
      description: "React is the library for web and native user interfaces. Build user interfaces out of individual pieces called components written in JavaScript.",
    },
    {
      title: "Quick Start",
      url: "https://react.dev/learn",
      description: "Installation - React Reference Overview - Describing the UI - ..."
    },
    {
      title: "Tutorial: Tic-Tac-Toe",
      url: "https://react.dev/learn/tutorial-tic-tac-toe",
      description: "Describing the UI - Thinking in React - Memo - ..."
    },
    {
      title: "Installation",
      url: "https://react.dev/learn/installation",
      description: "Installation · Start a new React project. If you want to build an ..."
    },
    {
      title: "Thinking in React",
      url: "https://react.dev/learn/thinking-in-react",
      description: "Thinking in React · Step 1: Break the UI into a component ..."
    },
    {
      title: "Start a New React Project",
      url: "https://react.dev/learn/start-a-new-react-project",
      description: "Start a New React Project. If you want to build a new app or a ..."
    },
    {
      title: "more results from react.dev",
      url: "https://www.google.com/search?q=react+site:react.dev&sca_esv=f7f0ca8bbe6f40fd&sa=X&ved=2ahUKEwibq_WArIOFAxViDHkGHTagBbsQrAN6BAgQEAE&biw=1664&bih=941&dpr=1.5",
      description: ""
    }
  ]);

} else if (term === "typescript") {
  setSearchResults([
    {
      title: "TypeScript",
      url: "https://typescriptlang.org",
      description: "TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.",
    },
    {
      title: "TypeScript Documentation",
      url: "https://www.typescriptlang.org/docs/",
      description: "Handbook - TS for the New Programmer - The Basics - JSX",
    },
    {
      title: "Documentation",
      url: "https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html",
      description: "By understanding how JavaScript works, TypeScript can build a ...",
    },
    {
      title: "Playground",
      url: "https://www.typescriptlang.org/play",
      description: "The Playground lets you write TypeScript or JavaScript online ...",
    },
    {
      title: "Download Install TypeScript",
      url: "https://www.typescriptlang.org/download",
      description: "via npm. You can use npm to install TypeScript globally, this ..."
    },
    {
      title: "Handbook",
      url: "https://www.typescriptlang.org/docs/handbook/intro.html",
      description: "The TypeScript Handbook is intended to be a comprehensive ...",
    },
    {
      title: "More results from typescriptlang.org »",
      url: "https://www.google.com/search?q=typescript+site:typescriptlang.org&sca_esv=f25fd1d09a6b1709&rlz=1C1VDKB_enUS1039US1039&sa=X&ved=2ahUKEwjnptCdqJCFAxWTvokEHVTjBSEQrAN6BAgqEAE&biw=1664&bih=941&dpr=1.5",
      description: "",
    },
    {
      title: "W3Schools TypeScript Introduction",
      url: "https://www.w3schools.com/typescript/typescript_intro.php",
      description: "TypeScript allows specifying the types of data being passed around within the code, and has the ability to report errors when the types don't match.",
    }
  ]);
}
};

const askResults: SearchResult[] = [
{
  title: "hi",
  url: "mrs.larsen.com",
  description: "something"
}
// Add "People Also Ask" results data here
];

return (
<div id="root">
  <div className="header">
    <Title name="Google" />
    <SearchBox onSearch={handleSearch} />
  </div>
  <div className="askResults">
    <SearchResultsList results={searchResults} />
    <AlsoAskSection results={askResults} />
  </div>
</div>
);
}

export default App;
