module.exports = {
  extends: [
    'semistandard'
  ],
  overrides: [
    {
      files: ['tests/**/*'],
      env: {
        jest: true
      }
    }
  ]
};
